
public class Konsolenausgabe2 { 
	
	public static void main(String[] args) {
		//Aufgabe 1
		System.out.printf("%5s \n","**");
		System.out.printf("%s %6s \n", "*", "*");
		System.out.printf("%s %6s \n", "*", "*");
		System.out.printf("%5s \n \n","**");
		
		//Aufgabe 2
		System.out.printf("%-5s %s %-	19s %s %4s \n", "0!", "=", " ", "=", "1" );
		System.out.printf("%-5s %s %-19s %s %4s \n", "1!", "=", "1", "=", "1" );
		System.out.printf("%-5s %s %-19s %s %4s \n", "2!", "=", "1 * 2", "=", "2" );
		System.out.printf("%-5s %s %-19s %s %4s \n", "3!", "=", "1 * 2 * 3", "=", "6" );
		System.out.printf("%-5s %s %-19s %s %4s \n", "4!", "=", "1 * 2 * 3 * 4", "=", "24" );
		System.out.printf("%-5s %s %-19s %s %4s \n \n", "5!", "=", "1 * 2 * 3 * 4 * 5", "=", "120" );
		
		//Aufgabe 3
		System.out.printf("\n%-12s|%10s \n", "Fahrenheit", "Celsius");
		System.out.printf("%s", "------------------------ \n");
		System.out.printf("%+-12d|%10.2f\n", -20, -28.89);
		System.out.printf("%+-12d|%10.2f\n", -10, -23.33);
		System.out.printf("%+-12d|%10.2f\n", 0, -17.78);
		System.out.printf("%+-12d|%10.2f\n", 20, -6.67);
		System.out.printf("%+-12d|%10.2f\n", 30, -1.11);
		
	}
}

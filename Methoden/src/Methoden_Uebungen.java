import java.util.Scanner;
public class Methoden_Uebungen {
	
	//Aufgabe 4
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		sc.close();
	}
	public static double v_wuerfel(double a) {
		return a*a*a;
	}
	public static double v_quader(double a, double b, double c) {
		return a*b*c;
	}
	public static double v_pyramide(double a,double h) {
		return a*a*h/3;
	}
	public static double v_kugel(double r) {
		double h=3.14159265359;
		return 4/3*r*r*r*h;
	}
	
	//Aufgabe 5
	public static double euro_dollar(double zahl1) {
		return zahl1 * 1.22;
	}
	public static double euro_yen(double zahl1) {
		return zahl1 * 126.5;
	}
	public static double euro_pfund(double zahl1) {
		return zahl1 * 0.89;
	}
	public static double euro_schweizer_franken(double zahl1) {
		return zahl1 * 1.08;
	}
	public static double euro_schwedische_kronen(double zahl1) {
		return zahl1 * 10.10;
	}
	
}

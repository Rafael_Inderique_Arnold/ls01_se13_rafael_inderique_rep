public class Taschenrechner {

	public static void main(String[] args) {
		double a=4,b=5,erg1,erg2,erg3,erg4,erg5;
		
		erg1 = plus(a,b);
		erg2 = minus(a,b);
		erg3 = mal(a,b);
		erg4 = durch(a,b);
		erg5 = quadrat(a);
		
		System.out.printf("%+.2f\n%+.2f\n%+.2f\n%+.2f\n%+.2f",erg1,erg2,erg3,erg4,erg5);

	}
	public static double plus(double a,double b) {
		return a+b;
	}
	public static double minus(double a,double b) {
		return a-b;
	}
	public static double mal(double a,double b) {
		return a*b;
	}
	public static double durch(double a,double b) {
		return a/b;
	}
	public static double quadrat(double a) {
		return a*a;
	}
	
}

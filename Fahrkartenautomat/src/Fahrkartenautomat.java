﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args) {

    	boolean endless = true;
    	while (endless == true) {
    	
        double zuZahlenderBetrag = 0; 
        double anzahlDerTickets;
        double rückgabebetrag;
        double eingezahlterGesamtbetrag = 0;
        double eingeworfeneMünze;
        char EURO = '€';
        
        
        zuZahlenderBetrag = fahrkartenbestellungErfassen();
        // Geldeinwurf
        eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
        // Fahrscheinausgabe
        fahrkartenAusgeben();
        
        // Rückgeldberechnung und -Ausgabe
        rueckgeldAusgeben(zuZahlenderBetrag, eingezahlterGesamtbetrag);
    	}
    }
    
    public static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	double zuZahlenderBetrag = 0; 
        boolean loop1 = true;
    	String welchesTicketIn = "";
        
    	String [] [] ticketArray = new String [10] [2];
    	ticketArray[0][0] = "Einzelfahrschein Berlin AB";
    	ticketArray[0][1] = "2.9";
    	ticketArray[1][0] = "Einzelfahrschein Berlin BC";
    	ticketArray[1][1] = "3.3";
    	ticketArray[2][0] = "Einzelfahrschein Berlin ABC";
    	ticketArray[2][1] = "3.6";
    	ticketArray[3][0] = "Kurzstrecke";
    	ticketArray[3][1] = "1.9";
    	ticketArray[4][0] = "Tageskarte Berlin AB";
    	ticketArray[4][1] = "8.6";
    	ticketArray[5][0] = "Tageskarte Berlin BC";
    	ticketArray[5][1] = "9";
    	ticketArray[6][0] = "Tageskarte Berlin ABC";
    	ticketArray[6][1] = "9.6";
    	ticketArray[7][0] = "Kleingruppen-Tageskarte Berlin AB";
    	ticketArray[7][1] = "23.6";
    	ticketArray[8][0] = "Kleingruppen-Tageskarte Berlin BC";
    	ticketArray[8][1] = "24.3";
    	ticketArray[9][0] = "Kleingruppen-Tageskarte Berlin ABC";
    	ticketArray[9][1] = "24.9";
    	double ticketpreis = 0;
        double ticketpreis1 = 3;
        String ticketname1 = "Einzelfahrschein Regeltarif AB";
        double ticketpreis2 = 1.9;
        String ticketname2 = "Einzelfahrschein Ermäßigungstarif AB";
        double ticketpreis3 = 8.8;
        String ticketname3 = "Tageskarte Regeltarif AB";
        double ticketpreis4 = 5.6;
        String ticketname4 = "Tageskarte Ermäßigungstarif AB";
        double ticketpreis5 = 25.5;
        String ticketname5 = "Kleingruppen-Tageskarte Regeltarif AB";
        
        while(loop1 == true) {
        System.out.println("Fahrkartenbestellvorgang:" +"\n=========================================");
        for(int i = 0; i < 10;i++) {
        	int ticketnummer = i + 1;
        	System.out.println("("+ ticketnummer +") " +ticketArray[i][0]);
        }
        System.out.print("========================================="+"\nIhre Wahl: ");                 
        
        welchesTicketIn = tastatur.next();
        //welchesTicketIn = tastatur.next().charAt(0); //erwartet CHar
        
        switch(welchesTicketIn)
 		  {
 		    case "1":
 		        System.out.println("Sie haben Ticket: "+ ticketArray[0][0] +" gewählt.");
 		        ticketpreis = ticketpreis + Double.parseDouble(ticketArray[0][1]);
 		        ticketpreis = AnzahlDerTickets(Double.parseDouble(ticketArray[0][1]));
 		      break;
 		    case "2":
 		    	System.out.println("Sie haben Ticket: "+ ticketArray[1][0] +" gewählt.");
 		    	ticketpreis = ticketpreis + Double.parseDouble(ticketArray[1][1]);
 		    	ticketpreis = AnzahlDerTickets(Double.parseDouble(ticketArray[1][1]));
 		      break;
 		    case "3":
 		    	System.out.println("Sie haben Ticket: "+ ticketArray[2][0] +" gewählt.");
 		    	ticketpreis = ticketpreis + Double.parseDouble(ticketArray[2][1]);
 		    	ticketpreis = AnzahlDerTickets(Double.parseDouble(ticketArray[2][1]));
 			  break;
 			case "4":
 				System.out.println("Sie haben Ticket: "+ ticketArray[3][0] +" gewählt.");
 				ticketpreis = ticketpreis + Double.parseDouble(ticketArray[3][1]);
 				ticketpreis = AnzahlDerTickets(Double.parseDouble(ticketArray[3][1]));
 			  break;
 			case "5":
 				System.out.println("Sie haben Ticket: "+ ticketArray[4][0] +" gewählt.");
 				ticketpreis = ticketpreis + Double.parseDouble(ticketArray[4][1]);
 				ticketpreis = AnzahlDerTickets(Double.parseDouble(ticketArray[4][1]));
 			  break;
 			case "6":
 				System.out.println("Sie haben Ticket: "+ ticketArray[5][0] +" gewählt.");
 				ticketpreis = ticketpreis + Double.parseDouble(ticketArray[5][1]);
 				ticketpreis = AnzahlDerTickets(Double.parseDouble(ticketArray[5][1]));
 			  break;
 			case "7":
 				System.out.println("Sie haben Ticket: "+ ticketArray[6][0] +" gewählt.");
 				ticketpreis = ticketpreis + Double.parseDouble(ticketArray[6][1]);
 				ticketpreis = AnzahlDerTickets(Double.parseDouble(ticketArray[6][1]));
 			  break;
 			case "8":
 				System.out.println("Sie haben Ticket: "+ ticketArray[7][0] +" gewählt.");
 				ticketpreis = ticketpreis + Double.parseDouble(ticketArray[7][1]);
 				ticketpreis = AnzahlDerTickets(Double.parseDouble(ticketArray[7][1]));
 			  break;
 			case "9":
 				System.out.println("Sie haben Ticket: "+ ticketArray[8][8] +" gewählt.");
 				ticketpreis = ticketpreis + Double.parseDouble(ticketArray[8][1]);
 				ticketpreis = AnzahlDerTickets(Double.parseDouble(ticketArray[8][1]));
 			case "10":
 				System.out.println("Sie haben Ticket: "+ ticketArray[9][0] +" gewählt.");
 				ticketpreis = ticketpreis + Double.parseDouble(ticketArray[9][1]);
 				ticketpreis = AnzahlDerTickets(Double.parseDouble(ticketArray[9][1]));
 			  break;
 			case "11":
 				System.out.println("Sie haben nun alle Fahrkarten ausgewählt.");
 				loop1 = false;
 			  break;
 			 default:
 				 System.out.println("Bitte wählen Sie ein Ticket aus.");
 		  }
         }
        
    	return ticketpreis;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag)
    	{
    	Scanner tastatur = new Scanner(System.in);
    	double eingezahlterGesamtbetrag = 0;
        char EURO = '€';
    	
        	while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        		{
        		double eingeworfeneMünze; 
        		System.out.printf("\n%s %.2f", "Noch zu zahlen:" ,  (zuZahlenderBetrag - eingezahlterGesamtbetrag));
        		System.out.print(" " + EURO + "\n");
        		System.out.print("Eingabe (mind. 5ct, höchstens 2 "+ EURO +"): ");
        		eingeworfeneMünze = tastatur.nextDouble();
        			if (eingeworfeneMünze <= 2 || eingeworfeneMünze <= 0.05) 
        			{
        				eingezahlterGesamtbetrag += eingeworfeneMünze;
        			}
        			else 
        			{ 
        				System.out.println("\nBitte werfen Sie passende Münzen ein.");
        			}		
        		}

        return eingezahlterGesamtbetrag;
    	}

    public static void fahrkartenAusgeben() {
    	 System.out.println("\nFahrschein wird ausgegeben");
         for (int i = 0; i < 25; i++) // i wird gleich 0 gesetzt, ausgeführt bis i nicht mehr kleiner 8 ist, i + 1 gemacht
         {
            System.out.print("=");
            try {
  			Thread.sleep(250);
  		  }
            catch (InterruptedException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		  }
         }
         System.out.println("\n");
    }

    public static void rueckgeldAusgeben(double zuZahlenderBetrag, double eingezahlterGesamtbetrag) {
    	char EURO = '€';
    	double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(rückgabebetrag > 0.0)
        {
     	   System.out.printf("%s %.2f","Der Rückgabebetrag in Höhe von " , rückgabebetrag);
     	   System.out.print(" " + EURO);
     	   System.out.println(" wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen // solange wie rückgabebetrag größer-gleich 0 ist, wird die Schleife ausgeführt
            {
         	  System.out.println("2 " + EURO);
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 "+EURO);
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 ct");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 ct");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 ct");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 ct");
  	          rückgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.\n\n");
    }
    
    public static double AnzahlDerTickets (double ticketpreis) {
    	double anzahlDerTickets = 0;
    	double zuZahlenderBetrag = 0;
    	boolean loop2 = true;
    	Scanner tastatur = new Scanner(System.in);
    	
    	
    	while(loop2==true)
        {
        	if(anzahlDerTickets >= 1 || anzahlDerTickets <= 10) 
        	{
        		System.out.print("Anzahl der Tickets: ");
        		anzahlDerTickets = tastatur.nextDouble();
        		zuZahlenderBetrag = anzahlDerTickets*ticketpreis;
        		loop2 = false;
        		break;
        	}
        	else
        	{
        		System.out.println("Bitte wählen Sie zwischen 1-10 Tickets.");
        	}
        }
    	return zuZahlenderBetrag;
    }

}

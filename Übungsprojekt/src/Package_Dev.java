import java.util.Scanner;

public class Package_Dev {

	public static void main(String[] args) { //Main Methode
		boolean loop1 = true, loop2 = true;
		while(loop2 == true) {
		String menue;
		menue = menue(" ");
		while(loop1 == true) {
		switch(menue) {
		case "A":
			tr();
			loop1 = false;
			break;
		case "B":
			tabelle_s();
			loop1 = false;
			break;
		case "C":
			tabelle_d();
			loop1 = false;
			break;
		case "D":
			tabelle_interaktiv();
			loop1 = false;
			break;
		default:
			break;
		}
		}
		
		}
		

	}
	
	public static void tr() { //Taschenrechner, der die Grundrechenarten mit Werten ausgibt
		Scanner sc = new Scanner(System.in);
		System.out.print("Sie werden nun aufgefordert Ihre Zahlen zu nennen.\nZahl 1: ");
		double a, b;
		a = sc.nextDouble();
		System.out.print("Zahl 2: ");
		b = sc.nextDouble();
		System.out.printf("\n%14s%s","Ergebnisse","\n--------------------\n");
		System.out.printf("%-4.2f%s%-4.2f%s%+-4.2f\n",a , " + " , b , " = " , (a+b));
		System.out.printf("%-4.2f%s%-4.2f%s%+-4.2f\n",b , " - " , a , " = " , (b-a));
		System.out.printf("%-4.2f%s%-4.2f%s%+-4.2f\n",a , " - " , b , " = " , (a-b));
		System.out.printf("%-4.2f%s%-4.2f%s%+-4.2f\n",a , " x " , b , " = " , (a*b));
		System.out.printf("%-4.2f%s%-4.2f%s%+-4.2f\n",b , " / " , a , " = " , (b/a));
		System.out.printf("%-4.2f%s%-4.2f%s%+-4.2f\n--------------------\n\n\n",a , " / " , b , " = " , (a/b));
	}
	
	public static String menue(String menuepunkt) { //Methode die Men�punkte auflistet und einen ausw�hlt
		Scanner sc = new Scanner(System.in);
		boolean loop1 = true;
		while(loop1 == true) {
		
		System.out.printf("\n%15s%12s%s%s%s%s%s%s%s","Men�","\n-------------------------","\nA: Taschenrechner (auto)","\nB: Tabelle (String)","\nC: Tabelle (Durchschnitt)","\nD: Tabelle (Interaktiv)","\n-------------------------","\nW�hlen Sie einen Punkt aus: ");
		menuepunkt = sc.next();
		switch(menuepunkt)
			{
			case "A":
				menuepunkt = "A";
				loop1 = false;
				break;
			case "B":
				menuepunkt = "B";
				loop1 = false;
				break;
			case "C":
				menuepunkt = "C";
				loop1 = false;
				break;
			case "D":
				menuepunkt = "D";
				loop1 = false;
				break;
			case "E":
				menuepunkt = "E";
				loop1 = false;
				break;
			default:
				System.out.println("Bitte geben Sie einen g�ltigen Wert ein.\n");
			}
		
		}
		return menuepunkt;
		
	}
	
	public static void tabelle_s() { //Tabelle nur Strings
		boolean loop1 = true;
		String zeilen=" ",spalten = " ";
		Scanner sc = new Scanner(System.in);
		while(loop1 == true) {	
			System.out.println("Wie viele Spalten soll ihre Tabelle haben? (min. 2, max. 4)");
			spalten = sc.next();
			System.out.println("Wie viele Zeilen soll ihre Tabelle haben? (min. 2, max. 4)");
			zeilen = sc.next();
			
			
			if ((zeilen.equals("2")||zeilen.equals("3")||zeilen.equals("4"))&&(spalten.equals("2")||spalten.equals("3")||zeilen.equals("4"))) {
				loop1 = false;
				break;
				}
			else {
				System.out.println("Bitte geben Sie einen g�ltigen Wert ein!");
				loop1 = true;
			
				}
			
			}
			if(spalten.equals("2")&&zeilen.equals("2")) {
				String str1,str2,str3,str4;
				System.out.print("\nBitte geben Sie Wert 1 Zeile 1 ein: ");
				str1 = sc.next();
				System.out.print("Bitte geben Sie Wert 2 Zeile 1 ein: ");
				str2 = sc.next();
				System.out.print("Bitte geben Sie Wert 1 Zeile 2 ein: ");
				str3 = sc.next();
				System.out.print("Bitte geben Sie Wert 2 Zeile 2 ein: ");
				str4 = sc.next();
				
				System.out.printf("\n|%s|%s|\n|%s|%s|",str1,str2,str3,str4);
				}
			
			else if(spalten.equals("2")&&zeilen.equals("3")) {
				String str1,str2,str3,str4,str5,str6;
				System.out.print("\nBitte geben Sie Wert 1 Zeile 1 ein: ");
				str1 = sc.next();
				System.out.print("Bitte geben Sie Wert 2 Zeile 1 ein: ");
				str2 = sc.next();
				System.out.print("Bitte geben Sie Wert 1 Zeile 2 ein: ");
				str3 = sc.next();
				System.out.print("Bitte geben Sie Wert 2 Zeile 2 ein: ");
				str4 = sc.next();
				System.out.print("Bitte geben Sie Wert 1 Zeile 3 ein: ");
				str5 = sc.next();
				System.out.print("Bitte geben Sie Wert 2 Zeile 3 ein: ");
				str6 = sc.next();
				
				System.out.printf("\n|%s|%s|\n|%s|%s|\n|%s|%s|",str1,str2,str3,str4,str5,str6);
				}
			else if(spalten.equals("2")&&zeilen.equals("4")) {
				String str1,str2,str3,str4,str5,str6,str7,str8;
				System.out.print("\nBitte geben Sie Wert 1 Zeile 1 ein: ");
				str1 = sc.next();
				System.out.print("Bitte geben Sie Wert 2 Zeile 1 ein: ");
				str2 = sc.next();
				System.out.print("Bitte geben Sie Wert 1 Zeile 2 ein: ");
				str3 = sc.next();
				System.out.print("Bitte geben Sie Wert 2 Zeile 2 ein: ");
				str4 = sc.next();
				System.out.print("Bitte geben Sie Wert 1 Zeile 3 ein: ");
				str5 = sc.next();
				System.out.print("Bitte geben Sie Wert 2 Zeile 3 ein: ");
				str6 = sc.next();
				System.out.print("Bitte geben Sie Wert 1 Zeile 4 ein: ");
				str7 = sc.next();
				System.out.print("Bitte geben Sie Wert 2 Zeile 4 ein: ");
				str8 = sc.next();
				
				System.out.printf("\n|%s|%s|\n|%s|%s|\n|%s|%s|\n|%s|%s|\n",str1,str2,str3,str4,str5,str6,str7,str8);
				}
			else if(spalten.equals("3")&&zeilen.equals("2")) {
				String str1,str2,str3,str4,str5,str6;
				System.out.print("\nBitte geben Sie Wert 1 Zeile 1 ein: ");
				str1 = sc.next();
				System.out.print("Bitte geben Sie Wert 2 Zeile 1 ein: ");
				str2 = sc.next();
				System.out.print("Bitte geben Sie Wert 3 Zeile 1 ein: ");
				str3 = sc.next();
				System.out.print("Bitte geben Sie Wert 1 Zeile 2 ein: ");
				str4 = sc.next();
				System.out.print("Bitte geben Sie Wert 2 Zeile 2 ein: ");
				str5 = sc.next();
				System.out.print("Bitte geben Sie Wert 3 Zeile 2 ein: ");
				str6 = sc.next();
				
				System.out.printf("\n|%s|%s|%s|\n|%s|%s|%s|\n",str1,str2,str3,str4,str5,str6);
				}
			else if(spalten.equals("3")&&zeilen.equals("3")) {
				String str1,str2,str3,str4,str5,str6,str7,str8,str9;
				System.out.print("\nBitte geben Sie Wert 1 Zeile 1 ein: ");
				str1 = sc.next();
				System.out.print("Bitte geben Sie Wert 2 Zeile 1 ein: ");
				str2 = sc.next();
				System.out.print("Bitte geben Sie Wert 3 Zeile 1 ein: ");
				str3 = sc.next();
				System.out.print("Bitte geben Sie Wert 1 Zeile 2 ein: ");
				str4 = sc.next();
				System.out.print("Bitte geben Sie Wert 2 Zeile 2 ein: ");
				str5 = sc.next();
				System.out.print("Bitte geben Sie Wert 3 Zeile 2 ein: ");
				str6 = sc.next();
				System.out.print("Bitte geben Sie Wert 1 Zeile 3 ein: ");
				str7 = sc.next();
				System.out.print("Bitte geben Sie Wert 2 Zeile 3 ein: ");
				str8 = sc.next();
				System.out.print("Bitte geben Sie Wert 3 Zeile 3 ein: ");
				str9 = sc.next();
				
				System.out.printf("\n|%s|%s|%s|\n|%s|%s|%s|\n|%s|%s|%s|\n",str1,str2,str3,str4,str5,str6,str7,str8,str9);
				}
			else if(spalten.equals("3")&&zeilen.equals("4")) {
				String str1,str2,str3,str4,str5,str6,str7,str8,str9,str10,str11,str12;
				System.out.print("\nBitte geben Sie Wert 1 Zeile 1 ein: ");
				str1 = sc.next();
				System.out.print("Bitte geben Sie Wert 2 Zeile 1 ein: ");
				str2 = sc.next();
				System.out.print("Bitte geben Sie Wert 3 Zeile 1 ein: ");
				str3 = sc.next();
				System.out.print("Bitte geben Sie Wert 1 Zeile 2 ein: ");
				str4 = sc.next();
				System.out.print("Bitte geben Sie Wert 2 Zeile 2 ein: ");
				str5 = sc.next();
				System.out.print("Bitte geben Sie Wert 3 Zeile 2 ein: ");
				str6 = sc.next();
				System.out.print("Bitte geben Sie Wert 1 Zeile 3 ein: ");
				str7 = sc.next();
				System.out.print("Bitte geben Sie Wert 2 Zeile 3 ein: ");
				str8 = sc.next();
				System.out.print("Bitte geben Sie Wert 3 Zeile 3 ein: ");
				str9 = sc.next();
				System.out.print("Bitte geben Sie Wert 1 Zeile 4 ein: ");
				str10 = sc.next();
				System.out.print("Bitte geben Sie Wert 2 Zeile 4 ein: ");
				str11 = sc.next();
				System.out.print("Bitte geben Sie Wert 3 Zeile 4 ein: ");
				str12 = sc.next();
				
				System.out.printf("\n|%s|%s|%s|\n|%s|%s|%s|\n|%s|%s|%s|\n|%s|%s|%s|\n",str1,str2,str3,str4,str5,str6,str7,str8,str9,str10,str11,str12);
				}
			else {
				System.out.println("ERROR");
				}
			
	}	

	public static void tabelle_d() { //Tabelle mit Doubles die Durchschnitt berechnet

		boolean loop1 = true;
		String zeilen=" ",spalten = " ";
		Scanner sc = new Scanner(System.in);
		while(loop1 == true) {	
			System.out.println("Wie viele Spalten soll ihre Tabelle (exklusive der Spalte f�r den Durchschnitt) haben? (min. 2, max. 4)");
			spalten = sc.next();
			System.out.println("Wie viele Zeilen soll ihre Tabelle haben? (min. 2, max. 4)");
			zeilen = sc.next();
			
			
			if ((zeilen.equals("2")||zeilen.equals("3")||zeilen.equals("4"))&&(spalten.equals("2")||spalten.equals("3")||zeilen.equals("4"))) {
				loop1 = false;
				break;
				}
			else {
				System.out.println("Bitte geben Sie einen g�ltigen Wert ein!");
				loop1 = true;
			
				}
			
			}
			if(spalten.equals("2")&&zeilen.equals("2")) {
				double str1,str2,str3,str4;
				System.out.print("\nBitte geben Sie Wert 1 Zeile 1 ein: ");
				str1 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 2 Zeile 1 ein: ");
				str2 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 1 Zeile 2 ein: ");
				str3 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 2 Zeile 2 ein: ");
				str4 = sc.nextDouble();
				
				System.out.printf("\n|%5.2f|%5.2f|= %5.2f|\n|%5.2f|%5.2f|= %5.2f|\n",str1,str2,((str1+str2)/2),str3,str4,((str3+str4)/2));
				}
			
			else if(spalten.equals("2")&&zeilen.equals("3")) {
				double str1,str2,str3,str4,str5,str6;
				System.out.print("\nBitte geben Sie Wert 1 Zeile 1 ein: ");
				str1 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 2 Zeile 1 ein: ");
				str2 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 1 Zeile 2 ein: ");
				str3 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 2 Zeile 2 ein: ");
				str4 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 1 Zeile 3 ein: ");
				str5 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 2 Zeile 3 ein: ");
				str6 = sc.nextDouble();
				
				System.out.printf("\n|%5.2f|%5.2f|= %5.2f|\n|%5.2f|%5.2f|= %5.2f|\n|%5.2f|%5.2f|= %5.2f|\n",str1,str2,((str1+str2)/2),str3,str4,((str3+str4)/2),str5,str6,((str5+str6)/2));
				}
			else if(spalten.equals("2")&&zeilen.equals("4")) {
				double str1,str2,str3,str4,str5,str6,str7,str8;
				System.out.print("\nBitte geben Sie Wert 1 Zeile 1 ein: ");
				str1 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 2 Zeile 1 ein: ");
				str2 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 1 Zeile 2 ein: ");
				str3 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 2 Zeile 2 ein: ");
				str4 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 1 Zeile 3 ein: ");
				str5 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 2 Zeile 3 ein: ");
				str6 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 1 Zeile 4 ein: ");
				str7 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 2 Zeile 4 ein: ");
				str8 = sc.nextDouble();
				
				System.out.printf("\n|%5.2f|%5.2f|= %5.2f|\n|%5.2f|%5.2f|= %5.2f|\n|%5.2f|%5.2f|= %5.2f|\n|% 5.2f|%5.2f|= %5.2f|\n",str1,str2,((str1+str2)/2),str3,str4,((str3+str4)/2),str5,str6,((str5+str6)/2),str7,str8,((str7+str8)/2));
				}
			else if(spalten.equals("3")&&zeilen.equals("2")) {
				double str1,str2,str3,str4,str5,str6;
				System.out.print("\nBitte geben Sie Wert 1 Zeile 1 ein: ");
				str1 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 2 Zeile 1 ein: ");
				str2 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 3 Zeile 1 ein: ");
				str3 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 1 Zeile 2 ein: ");
				str4 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 2 Zeile 2 ein: ");
				str5 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 3 Zeile 2 ein: ");
				str6 = sc.nextDouble();
				
				System.out.printf("\n|%5.2f|%5.2f|%5.2f|= %5.2f|\n|%5.2f|%5.2f|%5.2f|= %5.2f|\n",str1,str2,str3,((str1+str2+str3)/3),str4,str5,str6,((str4+str5+str6)/3));
				}
			else if(spalten.equals("3")&&zeilen.equals("3")) {
				double str1,str2,str3,str4,str5,str6,str7,str8,str9;
				System.out.print("\nBitte geben Sie Wert 1 Zeile 1 ein: ");
				str1 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 2 Zeile 1 ein: ");
				str2 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 3 Zeile 1 ein: ");
				str3 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 1 Zeile 2 ein: ");
				str4 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 2 Zeile 2 ein: ");
				str5 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 3 Zeile 2 ein: ");
				str6 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 1 Zeile 3 ein: ");
				str7 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 2 Zeile 3 ein: ");
				str8 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 3 Zeile 3 ein: ");
				str9 = sc.nextDouble();
				
				System.out.printf("\n|%5.2f|%5.2f|%5.2f|= %5.2f|\n|%5.2f|%5.2f|%5.2f|= %5.2f|\n|%5.2f|%5.2f|%5.2f|= %5.2f|\n",str1,str2,str3,((str1+str2+str3)/3),str4,str5,str6,((str4+str5+str6)/3),str7,str8,str9,((str7+str8+str9)/3));
				}
			else if(spalten.equals("3")&&zeilen.equals("4")) {
				double str1,str2,str3,str4,str5,str6,str7,str8,str9,str10,str11,str12;
				System.out.print("\nBitte geben Sie Wert 1 Zeile 1 ein: ");
				str1 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 2 Zeile 1 ein: ");
				str2 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 3 Zeile 1 ein: ");
				str3 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 1 Zeile 2 ein: ");
				str4 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 2 Zeile 2 ein: ");
				str5 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 3 Zeile 2 ein: ");
				str6 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 1 Zeile 3 ein: ");
				str7 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 2 Zeile 3 ein: ");
				str8 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 3 Zeile 3 ein: ");
				str9 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 1 Zeile 4 ein: ");
				str10 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 2 Zeile 4 ein: ");
				str11 = sc.nextDouble();
				System.out.print("Bitte geben Sie Wert 3 Zeile 4 ein: ");
				str12 = sc.nextDouble();
				
				System.out.printf("\n|%5.2f|%5.2f|%5.2f|= %5.2f|\n|%5.2f|%5.2f|%5.2f|= %5.2f|\n|%5.2f|%5.2f|%5.2f|= %5.2f|\n|%5.2f|%5.2f|%5.2f|= %5.2f|\n",str1,str2,str3,((str1+str2+str3)/3),str4,str5,str6,((str4+str5+str6)/3),str7,str8,str9,((str7+str8+str9)/3),str10,str11,str12,((str10+str11+str12)/3));
				}
			else {
				System.out.println("ERROR");
				}
	}
	
	public static void tabelle_interaktiv() { //Tabelle wo man Werte einsetzt
		boolean loop1 = true;
		String zeilen=" ",spalten = " ";
		Scanner sc = new Scanner(System.in);
		while(loop1 == true) {	
			System.out.println("Wie viele Spalten soll ihre Tabelle (exklusive der Berechnungsspalte) haben? (min. 2, max. 4)");
			spalten = sc.next();
			System.out.println("Wie viele Zeilen soll ihre Tabelle haben? (min. 2, max. 4)");
			zeilen = sc.next();
			
			
			if ((zeilen.equals("2")||zeilen.equals("3")||zeilen.equals("4"))&&(spalten.equals("2")||spalten.equals("3")||zeilen.equals("4"))) {
				loop1 = false;
				break;
				}
			else {
				System.out.println("Bitte geben Sie einen g�ltigen Wert ein!");
				loop1 = true;
			
				}
		}
		
		if(spalten.equals("2")&&zeilen.equals("2")) {
			double str1,str2,str3,str4;
			System.out.print("|");
			str1 = sc.nextDouble();
			System.out.print("|");
			str2 = sc.nextDouble();
			System.out.print("|");
			str3 = sc.nextDouble();
			System.out.print("|");
			str4 = sc.nextDouble();
			System.out.print("|");
			
			System.out.printf("\n|%5.2f|%5.2f|\n|%5.2f|%5.2f|\n",str1,str2,str3,str4);
			}
		
		else if(spalten.equals("2")&&zeilen.equals("3")) {
			
			}
		else if(spalten.equals("2")&&zeilen.equals("4")) {
			
			}
		else if(spalten.equals("3")&&zeilen.equals("2")) {
			
			}
		else if(spalten.equals("3")&&zeilen.equals("3")) {
			
			}
		else if(spalten.equals("3")&&zeilen.equals("4")) {
			
			}
		else {
			System.out.println("ERROR");
			}
	}
}

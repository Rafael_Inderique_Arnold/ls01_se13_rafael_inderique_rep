
public class Welt_der_Zahlen {

	public static void main(String[] args) {
		/**
		  *   Aufgabe:  Recherechieren Sie im Internet !
		  * 
		  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
		  *
		  *   Vergessen Sie nicht den richtigen Datentyp !!
		  *
		  *
		  * @version 1.0 from 21.08.2019
		  * @author << Rafael Inderique Arnold >>
		  */

		    
		    /*  *********************************************************
		    
		         Zuerst werden die Variablen mit den Werten festgelegt!
		    
		    *********************************************************** */
		    // Im Internet gefunden ?
		    // Die Anzahl der Planeten in unserem Sonnesystem                    
		    byte anzahlPlaneten = 8;
		    
		    // Anzahl der Sterne in unserer Milchstra�e
		    long anzahlSterne = 260000000001l;
		    
		    // Wie viele Einwohner hat Berlin?
		    int bewohnerBerlin = 3700000;
		    
		    // Wie alt bist du?  Wie viele Tage sind das?
		    
		    byte alterJahre = 17;
		    int alterTage = 6544; //Stand 02.11.2021
		    
		    // Wie viel wiegt das schwerste Tier der Welt?
		    // Schreiben Sie das Gewicht in Kilogramm auf!
		    int gewichtKilogramm = 130000;
		    
		    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
		    int flaecheGroessteLand = 17098242;
		    
		    // Wie gro� ist das kleinste Land der Erde?
		    
		    double flaecheKleinsteLand = 0.44; 
		    
		    
		    
		    
		    /*  *********************************************************
		    
		         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
		    
		    *********************************************************** */
		    
		    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
		    
		    System.out.println("Anzahl der Sterne in Milliarden: " + anzahlSterne);
		    
		    System.out.println("Anzahl der Bewohner von Berlin: " + bewohnerBerlin);
		    
		    System.out.println("Mein Alter in Jahren am 02.11.2021: " + alterJahre);
		    
		    System.out.println("Mein Alter in Tagen am 02.11.2021: " + alterTage);
		    
		    System.out.println("Das Gewicht des schwersten Tiers der Welt: " + gewichtKilogramm);
		    
		    System.out.println("Die Fl�che des gr��ten Landes: " + flaecheGroessteLand);
		    
		    System.out.println("Die Fl�che des kleinsten Landes: " + flaecheKleinsteLand);
		    
		    System.out.println(" *******  Ende des Programms  ******* ");
		    



	}

}

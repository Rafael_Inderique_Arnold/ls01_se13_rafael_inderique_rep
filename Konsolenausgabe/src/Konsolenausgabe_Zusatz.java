import java.util.Scanner;

public class Konsolenausgabe_Zusatz {

	public static void main(String[] args) {
		
		//Aufgabe 2
		Scanner tastatur = new Scanner(System.in);
		/*
		double einzahlung = 0;
		double zinsen = 0;
		double jahre = 0;
		double summe;
		
		System.out.printf("%-18s%s", "Einzahlung in Euro",": ");
		einzahlung = tastatur.nextDouble();
		System.out.printf("%-18s%s", "Zinsen in Prozent",": ");
		zinsen = tastatur.nextDouble();
		System.out.printf("%-18s%s", "Jahre",": ");
		jahre = tastatur.nextDouble();
		//Einzahlung * Zinsen * Jahre
		summe = (einzahlung)+(einzahlung * (zinsen/100))*jahre;
		System.out.printf("\n%s%.2f%s","Der Investor hat insgesamt ", summe,"€ erhalten.");
		*/
		//Aufgabe 6
		double kursgebühren = 9;
		double rabatt_schueler = 0.04;
		double anzahl_pro_abend; 
		double anzahl_kursabende;
		double preis_schueler;
		double preis_gesamt;
		
		System.out.printf("\n\n%-21s%s","Stundenzahl pro Abend",": ");
		anzahl_pro_abend = tastatur.nextDouble();
		
		System.out.printf("%-21s%s","Zahl der Kursabende",": ");
		anzahl_kursabende = tastatur.nextDouble();
		
		preis_schueler = rabatt_schueler*((anzahl_pro_abend*anzahl_kursabende)*kursgebühren);
		preis_gesamt = ((anzahl_pro_abend*anzahl_kursabende)*kursgebühren);
		
		System.out.printf("%-21s%s%.2f\n","Preis Normal",": ",preis_gesamt);
		System.out.printf("%-21s%s%.2f","Preis Schueler",": ",preis_schueler);
		
		tastatur.close();
	}

}

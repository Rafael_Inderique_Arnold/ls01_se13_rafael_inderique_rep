public class Konsolenausgabe {

	public static void main(String[] args) {

		//Aufgabe 1
		System.out.println("Das ist ein Beispielsatz. Ein \"Beispielsatz\" ist das.");
		System.out.println("Das ist ein Beispielsatz. "  + "\n Ein \"Beispielsatz\" ist das.");
		//Das ist ein Kommentar.
		
		// Der Unterschied zwischen println und print ist, dass bei println nach dem Ende des Inhalts von println eine neue Zeile begonnen wird.
		
		//Aufgabe 2
		
		System.out.println("             *");
		System.out.println("            ***");
		System.out.println("           *****");
		System.out.println("          *******");
		System.out.println("         *********");
		System.out.println("        ***********");
		System.out.println("            ***");
		System.out.println("            ***");
		
		//Aufgabe 3
		
		double a = 22.4234234;
		double b = 111.2222;
		double c = 4.0;
		double d = 1000000.551;
		double e = 97.34;
		
		System.out.printf("%.2f\n",a);
		System.out.printf("%.2f\n",b);
		System.out.printf("%.2f\n",c);
		System.out.printf("%.2f\n",d);
		System.out.printf("%.2f\n",e);
		
		
		
	}

}
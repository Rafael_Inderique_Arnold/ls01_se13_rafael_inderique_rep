import java.util.Scanner;

public class InputScanner_Switch {

	public static void main(String[] args) {
		
		//Scanner - Erster Versuch mit Inputs
		
		/*
		Scanner Tastatur = new Scanner(System.in);  
		
		System.out.print("Bitte geben Sie eine Gleitkommazahl ein: ");
		double zahla = Tastatur.nextDouble(); //Doubles
		
		System.out.print("\nBitte geben Sie eine Ganzzahl ein: ");
		int zahlb = Tastatur.nextInt();
		Tastatur.close();
		
		Tastatur = new Scanner(System.in);  
		//Irgendwie wird das Leerzeichen übernommen und man kann irgendwie den Puffer der Tastatur übernehmen
		
		System.out.print("\nBitte geben Sie einen String ein: ");
		String zahlF = Tastatur.nextLine();
		//System.out.print("\n" + zahla + zahlb );
		System.out.print("\n"+zahlF);
		Tastatur.close();
		*/
		
		//Switch
		
		Scanner sc = new Scanner(System.in);
		
		String wetter;
		System.out.println("Sehen Sie Sonne oder Regen?");  
		wetter = sc.next();
		switch(wetter)
		  {
		    case "Regen":
		      System.out.println("Nimm den Schirm mit!");      
		      break;
		    case "Sonne":
		      System.out.println("Be happy!");
		      break;
		    case "regen":
			      System.out.println("Nimm den Schirm mit!");      
			  break;
			case "sonne":
			      System.out.println("Be happy!");
			  break;
		  }
	}

}

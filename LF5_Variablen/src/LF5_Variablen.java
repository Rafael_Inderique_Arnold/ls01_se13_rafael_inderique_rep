
public class LF5_Variablen { //Klassennamen wird immer gro� geschrieben

	public static void main(String[] args) { //main Methode, Einsprungspunkt/EInstiegspunkt bei der Ausf�hrung
		/*
		 * erste Arbeit mit Variablen
		 * 
		 * @version 1.0 vom 27.09.2021
		 */
		
		int i; //Variablendeklaration, Variablennamen IMMER klein
		i = 5; //Variableninitialisierung = wenn Startwert zugewiesen wird
		int o = 5; //Kann auch gleichzeitig geschehen
		
		//Berechnung
		i = i + o; //rechts immer Rechnung und = ist Zuweisungsoperator
		System.out.println(i);
		System.out.println(i+o);
		
	
		

	} //Ende von main

} // Ende von Class Variablen
